#!/usr/bin/env python3
#
# Copyright 2022 Codethink Ltd
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# raw file archive using git-lfs frontend for git-fast-import

import argparse
import os
import subprocess
import sys
import contextlib
import time
import hashlib
import shutil

def is_valid_executable(path):
    dirname, basename = os.path.split(path)
    if dirname:
        return os.path.isfile(path) and os.access(path, os.X_OK)
    if not dirname: # bare name of an executable, search in PATH
        for pathstr in os.environ["PATH"].split(os.pathsep):
            newpath = os.path.join(pathstr, basename)
            if os.path.isfile(newpath) and os.access(newpath, os.X_OK):
                return True
        return False

@contextlib.contextmanager
def fast_import_stream(gitcmd, repository_path):
    with subprocess.Popen([gitcmd, "-C", repository_path, "fast-import", "--quiet"],
                          stdin=subprocess.PIPE) as fast_import_process:
        yield fast_import_process.stdin
        fast_import_process.stdin.close()
        retcode = fast_import_process.wait()
        if retcode != 0:
            sys.stderr.write(f"Failure in git fast-import (code {retcode})")
            sys.exit(1)


def last_commit_sha(gitcmd, branch_name, repository_path):
    # TODO: Investigate whether `from refs/heads/branch^0` means I can skip this
    branch_ref = f"refs/heads/{branch_name}"
    ret = subprocess.run(
        [gitcmd, "-C", repository_path, "rev-parse", branch_ref],
        stdout=subprocess.PIPE, universal_newlines=True
    )
    if ret.returncode == 128:
        # invalid object name, branch doesn't exist yet
        return None
    if ret.returncode != 0:
        sys.stderr.write(f"Parsing branch ref had unexpected return code {ret.returncode}: {ret.stderr}\n")
        sys.exit(1)
    out = ret.stdout.strip()
    return None if out == branch_ref else out


def write_gitattributes(fast_import, track_expression, last_commit, branch, author):
    commit_time = int(time.time())
    if last_commit:
        fromline = f"from {last_commit}\n"
    else:
        fromline = ""
    commit = (
        f"commit refs/heads/{branch}\n"
        f"committer {author} {commit_time} +0000\n"
        "data <<EOM\n"
        "Write gitattributes for LFS\n"
        "EOM\n"
        f"{fromline}"
        "M 100644 inline .gitattributes\n"
        "data <<EOM\n"
        f"{track_expression}\n"
        ".gitattributes filter diff merge text=auto\n"
        "EOM\n"
        "\n"
    )
    fast_import.write(commit.encode("utf-8"))

def write_files(fast_import, last_commit, branch, author, commit_message, filepaths, lfscmd, subdir, repository_path):
    commit_time = int(time.time())
    if last_commit:
        fromline = f"from {last_commit}\n"
    else:
        fromline = ""
    if not commit_message:
        commit_message = f"Commit files {', '.join(filepaths)}"
    commit = (
        f"commit refs/heads/{branch}\n"
        f"committer {author} {commit_time} +0000\n"
        "data <<EOM\n"
        f"{commit_message}\n"
        "EOM\n"
        f"{fromline}"
    )
    for filepath in filepaths:
        # Add the files into the LFS subdir
        with open(filepath, "rb") as f:
            # Slightly wasteful, since the shasum is also part of the pointer digest, but that's
            # harder to parse
            shasum = hashlib.sha256(f.read()).hexdigest()
        out_dir = os.path.join(repository_path, "lfs", "objects", shasum[0:2], shasum[2:4])
        os.makedirs(out_dir, exist_ok=True)
        shutil.copyfile(filepath, os.path.join(out_dir, shasum))

        # Commit the pointers to those files
        ret = subprocess.run(
            [lfscmd, "pointer", "--file", filepath],
            stdout = subprocess.PIPE, universal_newlines=True,
        )
        if ret.returncode != 0:
            sys.stderr.write(f"Generating a pointer digest for file {filepath} failed: {ret.stderr}\n")
            sys.exit(1)
        pointer_digest = ret.stdout
        commit_path = os.path.join(subdir, filepath)
        file_entry = (
            f"M 100644 inline {commit_path}\n"
            f"data {len(pointer_digest)}\n"
            f"{pointer_digest}\n"
        )
        commit += file_entry
    commit += "\n"
    fast_import.write(commit.encode("utf-8"))

parser = argparse.ArgumentParser(description="Commits files directly into a git repository using git-lfs")
parser.add_argument("files", nargs="+", help="The files to be committed into the git repository")
parser.add_argument("repository_path", help="The path to the git repository to have files committed")
parser.add_argument("-b", "--branch", help="The name of the branch to commit the changes to", default="master")
parser.add_argument("-m", "--commit-message", help="The message to use when committing these files instead of the default")
parser.add_argument("-d", "--subdir", help="Subdirectory of the repository to add the files to", default="")
parser.add_argument("--author", help="Override the author identity with this (format 'NAME <E-MAIL>')", default="LFS Fastimport <lfsfastimport@codethink.co.uk>")
parser.add_argument("--track", help="Installs a .gitattributes file that tracks these changes", action='store_true', default=False)
parser.add_argument("--track-file-pattern", help="Specifies the file pattern that should be tracked by LFS", default="*")
parser.add_argument("--git-binary", help="Alternative binary to call instead of 'git'", default="git")
parser.add_argument("--git-lfs-binary", help="Alternative binary to call instead of 'git-lfs'", default="git-lfs")

args = parser.parse_args()
if not is_valid_executable(args.git_binary):
    sys.stderr.write(f"Could not find valid executable at '{args.git_binary}', please check git is installed\n")
    sys.exit(1)

if not is_valid_executable(args.git_lfs_binary):
    sys.stderr.write(f"Could not find valid executable at '{args.git_lfs_binary}', please check git-lfs is installed\n")

for f in args.files:
    if f.startswith("/"):
        sys.stderr.write(f"Cannot import {f}, files must have a relative path")
        sys.exit(1)

# TODO: Check whether the files to be added are caught by globbing expressions in gitattributes
track_expression = f"{args.track_file_pattern} filter=lfs diff=lfs merge=lfs -text"

if not args.track:
    cmd = [args.git_binary, "-C", args.repository_path, "show", f"{args.branch}:.gitattributes"]
    result = subprocess.run(cmd, stdout=subprocess.PIPE, universal_newlines=True)
    if result.returncode == 128:
        # invalid object name, branch doesn't exist yet so you need to track.
        sys.stderr.write(f"Branch '{args.branch}' of repo '{args.repository_path}' does not appear to be configured for LFS. Consider re-running this command with '--track' or manually adding your own rules to the branch's .gitattributes\n")
        sys.exit(1)
    if result.returncode != 0:
        sys.stderr.write(f"Error (return code {result.returncode}) while calling \"{' '.join(cmd)}\"\n"
                         f"stdout: {result.stdout}\n stderr: {result.stderr}\n")
        sys.exit(1)
    if track_expression not in result.stdout:
        sys.stderr.write(f"Branch '{args.branch}' of repo '{args.repository_path}' does not appear to be configured for LFS. Consider re-running this command with '--track' or manually adding your own rules to the branch's .gitattributes\n")
        sys.exit(1)

# Run `git lfs install` to make sure the smudge filters are set up in your config
cmd = [args.git_lfs_binary, "install"]
result = subprocess.run(cmd, cwd=args.repository_path)
assert result.returncode == 0

last_commit = last_commit_sha(args.git_binary, args.branch, args.repository_path)
with fast_import_stream(args.git_binary, args.repository_path) as fast_import:
    if args.track:
        # TODO: Incremental track
        #   this requires constructing a diff for each change and committing that, perhaps with `diff -u`
        write_gitattributes(fast_import, track_expression, last_commit, args.branch, args.author)
        last_commit = None
    write_files(fast_import, last_commit, args.branch, args.author, args.commit_message, args.files, args.git_lfs_binary, args.subdir, args.repository_path)

    
